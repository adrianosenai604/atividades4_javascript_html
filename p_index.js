//Processamento dos dados do formulário "index.html"

/*getElementById é um método do objeto document em JavaScript que permite acessar um elemento HTML específico na página pelo seu atributo id. Ele retorna uma referência ao elemento HTML com o id correspondente.*/
const formulario = document.getElementById("formulario1");

/*addEventListener: é utilizado em JavaScript para adicionar um ouvinte de eventos a um elemento HTML*/
formulario.addEventListener("submit", function (evento) {
  evento.preventDefault(); /*previne o comportamento padrão de um elemento HTML em resposta a um evento*/

  //variáveis para tratar os dados recebidos do formulário
  let nome = document.getElementById("nome").value;
  let email = document.getElementById("email").value;

  //Exibe um alerta com os dados coletados pelo formulário
  alert(`Nome: ${nome} \nE-mail: ${email}`);

  //Acessando o elemenro "resultado" que é o campo de texto do html
  let nameInput = document.getElementById("resultado");

  nameInput.style.width = "auto";//definindo a largura automática do campo texto
  nameInput.value = `Nome: ${nome} \n E-mail: ${email}`;//Atualizando o conteúdo do campo de texto
  nameInput.style.width = nameInput.scrollWidth + "px";//Atualizando o tamanho do campo de texto

  //Acessando o elemento "message" que é o campo "textarea" do html
  let messageInput = document.getElementById("message");
  messageInput.style.height = "auto";//definindo a altura automática do "textarea"
  messageInput.style.width = "auto";//definindo a largura automática do "textarea"
  messageInput.value = `Nome: ${nome} \nE-mail: ${email}`;//Atualizando o conteúdo do "textarea"  

  messageInput.style.width = messageInput.scrollWidth + "px"; //Atualizando a largura do "textarea"
  messageInput.style.height = messageInput.scrollHeight + "px"; //Atualizando a altura do "textarea"  
});
